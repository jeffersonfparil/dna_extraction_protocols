# Plant DNA extraction using sorbitol wash, SDS buffer, and magnetic bead purification

## List of chemicals and their functions in the protocol

- **β-Mercaptoethanol** - denatures nucleases by reducing their disulfide bonds.
- **Ethanol (C₂H₅OH)** - is a hydrophilic ("water hungry") DNA washing agent.
- **Ethylenediaminetetraacetic acid (EDTA)** - chelates divalent ions which prevents nuclease activity (EDTA has more affinity to magnesium ions while EGTA (ethylene glycol-bis(β-aminoethyl ether)-N,N,N',N'-tetraacetic acid) has higher affinity to calcium ions).
- **Liquid nitrogen (N₂ at −160°C)** - is an inert cryogenic fluid used to break the plant cell wall to expose the cell membrane during tissue homogenisation or grinding.
- **Polyethylene glycol (PEG)** - is a hydrophilic ("water hungry") polyether compound used to "crowd-out" or drive the DNA out of the aquaeous solution.
- **Polyvinylpyrrolidone 40 (PVP 40)** - binds to polyphenols and prevents their oxidation (oxidized polyphenols binds to DNA which results in brown discoloration)
- **Potassium acetate (CH₃CO₂K)** - ionically binds to sodium dodecyl sulfate (SDS), SDS-protein complexes, and SDS-polysaccharide complexes leading to their precipitation. It also reduces the pH of the solution which induces the complexing of Proteinase K and RNAse A with SDS.
- **Proteinase K** - is a broad-spectrum protease (cleaves peptide bonds adjacent to the carboxylic group of aliphatic and aromatic amino acids). It does not require calcium ions for activity (only for stability) and therefore can work even in the presence of EDTA. Its activity is enhanced in the presence of at most 2% sodium dodecyl sulfate (SDS) at pH 8.
- **RNAse A** - cleaves single-stranded RNA (cleaves after pyrimidine nucleotides). Its activity is not dependent on metal ion cofactors, hence works in the presence of EDTA. It also works in the presence of sodium dodecyl sulfate (SDS) as long as the pH is high ([higher than pH 3.3](https://pubmed.ncbi.nlm.nih.gov/15949923/), at this low pH it forms a complex with SDS).
- **Sodium chloride (NaCl)** - the sodium ions (Na+) ionically binds to the negatively charged phosphate groups of the DNA backbone which induces precipitation in the presence of "water-hungry" alcohol or polyethylene glycol (PEG). This precipitation process converts DNA from a long water-coated polymer into a water-stripped condensed "sticky ball". These "sticky balls" of DNA can aggragate together after centrifugation or stick to other surfaces it collides with like the surfaces of magnetic beads via hydrogen bonding (not ionic bonding).
- **Sodium dodecyl sulfate (SDS)** - lyses cell membranes, and denatures proteins thereby dissociating nucleic acid-protein complexes.
- **Sorbitol (C₆H₁₄O₆, D-sorbitol, D-glucitol)** - removes some polyphenols and polysaccharides by drawing out the cytosol without disrupting the cell membrane.
- **Tris(hydroxymethyl)aminomethane hydrochloride (Tris)** - acts as buffer to keep the pH of various solutions stable at around pH 8 ± 1.
- **Water (H₂O)** - is a polar solvent. Various molecular-grade forms exist including nuclease-free water (NFW), filtered and deionized water (Milli-Q), nanopure water (nH₂O), or double distilled water (ddH₂O).

## Solutions and buffers

- **Stock solutions**:

| Reagent                                                 | Molecular mass (g/mol) | Preparation (autoclave & store at room temperature)                                                        |
|:--------------------------------------------------------|:----------------------:|:-----------------------------------------------------------------------------------------------------------|
| 0.25M EDTA (ethylenediaminetetraacetic acid)            |  292.24        | 29.22g in 320ml H₂O with 12g NaOH; adjust to pH8 with NaOH; volume up 400ml                                |
| 5M potassium acetate (CH₃CO₂K)                          |   98.14        | 49.07g in 80ml H₂O; volume up 100ml                                                                        |
| 1M Tris (tris(hydroxymethyl)aminomethane hydrochloride) |  121.14        | 60.57g in 400ml H₂O; adjust to pH8 with HCl; volume up 500ml                                               |
| 5% (w/v) PVP 40 (polyvinylpyrrolidone 40)               |  40,000        | 10g in 160ml H₂O; volume up 200ml                                                                          |
| 5M NaCl  (sodium chloride)                              |    58.44       | 146g in 400ml H₂O; volume to 500ml                                                                         |
| 10% (w/v) SDS (sodium dodecyl sulfate)                  |  288.37        | 50g in 400ml H₂O; volume up 500ml; dissolve at 60°C; **do not autoclave or will precipitate irreversibly** |
| 70% (v/v) ethanol                                       |  46.07         | 700ml in 300ml H₂O; **do not autoclave**                                                                   |

- **SDS lysis buffer** (prepare on the day of use; 750μl/2ml-prep or 20ml/50ml-prep):

| Reagent                 | Final concentration | Amount to add for 250 ml |
|:------------------------|--------------------:|-------------------------:|
| PVP 40 (5% w/v)         |                  1% |                    50 ml |
| NaCl (5M)               |              0.5 M  |                    25 ml |
| Tris (1 M, pH 8)        |              100 mM |                    25 ml |
| EDTA (0.25 M, pH 8)     |              50 mM  |                    50 ml |
| SDS (10% w/v)           |                  2% |                    50 ml |
| Water (ddH₂O or Milli-Q)|                   - |                    50 ml |

- **Sorbitol wash solution** (store at 4°C or autoclave and store at room temperature; aliquot 1.3ml/2ml-prep or 33ml/50ml-prep):

| Reagent                  | Final concentration | Amount to add for 500 ml |
|:-------------------------|--------------------:|-------------------------:|
| D-Sorbitol (182.17g/mol) |              350 mM |                  31.88 g |
| PVP 40 (5% w/v)          |                  1% |                   100 ml |
| Tris (1 M, pH 8)         |              100 mM |                    50 ml |
| EDTA (0.25 M, pH 8)      |                5 mM |                    10 ml |
| Water (ddH₂O or Milli-Q) |                   - |      volume up to 500 ml |

- **1% β-Mercaptoethanol** (prepare under a fume hood on the day of use; 13μl/2ml-prep or 330μl/50ml prep):

| Reagent                        | Final concentration | Amount to add for 1 ml |
|:-------------------------------|--------------------:|-----------------------:|
| β-Mercaptoethanol (78.13g/mol) |            1% (v/v) |                  10 μl |
| Water (ddH₂O or Milli-Q)       |                   - |                 990 μl |

- **Binding buffer** (mix throroughly to make a clear solution, autoclave, shake/mix to redissolve the precipitated PEG, and store at room temperature):

| Reagent                              | Final concentration | Amount to add for 500 ml |
|:-------------------------------------|--------------------:|-------------------------:|
| Polyethylene glycol (PEG, 8000g/mol) |           20% (w/v) |                    100 g |
| NaCl (58.44g/mol)                    |                 3 M |                  87.66 g |
| Water (ddH₂O or Milli-Q)             |                   - |      volume up to 500 ml |

## Consumable materials and equipment

- **Nuclease-free water (NFW)** and Milli-Q water or nanopure water 
- **Liquid nitrogen**
- **Magnetic bead suspension** (1 mg beads / ml of buffer)
    + Buffer: 18% PEG, 1M NaCl, 10mM Tris pH8, 1mM EDTA pH8, & 0.05% Tween 20
    + Can be prepared with 50mg/ml Sera-Mag SpeedBead [ThermoFisher Scientific product 65152105050250](https://www.sigmaaldrich.com/catalog/product/sigma/ge24152105050250?lang=en&region=AU&gclid=Cj0KCQiA6Or_BRC_ARIsAPzuer_YhiEnsKTwMNWg78YNS-RjjenkhiD4kwwti3C96pJoAOf0ahMbEqUaAii3EALw_wcB))
    + Can also be procured pre-made as [Macherey-Nagel™ NucleoMag™ NGS Clean-up and Size Select](https://www.fishersci.co.uk/shop/products/nucleomag-ngs-clean-up-size-select/15889167) and [Ampure XP](https://www.beckman.com.au/reagents/genomic/cleanup-and-size-selection/pcr)
- **Proteinase K** (20 mg/mL; [NEB P8107S](https://www.nebiolabs.com.au/products/p8107-proteinase-k-molecular-biology-grade); do not vortex since the enzymes are vulnerable to mechanical denaturation)
- **RNAse A** (PureLink 20 mg/mL; [Thermo Fisher Scientific 12091039](https://www.thermofisher.com/order/catalog/product/12091039#/12091039); do not vortex since the enzymes are vulnerable to mechanical denaturation)
- Mortar and pestle (for 50ml-prep) or ball bearings and TissueLyser (or equivalent for 2ml-prep)
- Spatula (small)
- 50 ml Falcon tubes (for 50ml-prep) or 2ml Eppendorf tubes (for 2ml-prep) (3 sets of tubes)
- 1.5 ml Eppendorf tubes (1 to 2 sets of tubes)
- Pipettes and tips (p10, p100 & p1000)
- Water bath or heat block for 2 ml or 50 ml tubes
- Centrifuge (temperature-controlled)
- Fume hood

## Procedure

### I. Prepration of buffers and solutions
1. Prepare the **stock solutions**, **sorbitol wash solution**, and **binding buffer**.
2. Prepare the **SDS lysis buffer** (heat up 10% SDS stock solution to redissolve any precipitate), **1% β-Mercaptoethanol**, and 70% ethanol on the day of DNA extraction.
3. Pre-heat the **SDS lysis buffer** at 55°C.

### II. Homogenisation
4. Grind the plant tissues under liquid N₂ into a fine powder using mortar and pestle or ball bearings and TissueLyser (1-3 ball bearings per 2ml tube for 2min at 25Hz).
5. Transfer into 2 ml or 50 ml tubes (tubes set 1).

### III. Sorbitol wash
6. Aliquot the appropriate amount of **sorbitol wash solution**, i.e. for every sample 1.3 ml for 2ml-prep or 33 ml for 50ml-prep.
7. Add 10 μl of **1% β-Mercaptoethanol** for every 1 ml of **sorbitol wash solution**.
8. Add 1.3 ml or 33 ml (depending on the prep size) of the resulting **sorbitol wash solution** (mixed with **1% β-Mercaptoethanol**) to each of the homogenised tissue sample.
9. Mix throroughly by inversion, shaking, and vortexing.
10. Centrifuge at 5,000 rcf for 5 minutes at room temperature (This can be modified to 2,500 rcf for 10 min at room temperature to avoid the ball bearings rupturing the tubes).
11. Keep the pellet and remove the supernatant by decanting and pipetting (The supernatant should be cloudy and light yellow or light brown. It is fine to lose some of the floating plant tissues).
12. If the supernatant is viscous and dark in colour, repeat the sorbitol wash (i.e. go back to step 6).

### IV. Cell lysis
13. Add 10 μl of **RNAse A** for every 1 ml of the pre-heated **SDS lysis buffer**.
14. Add 750 μl or 20 ml (depending on prep size) of the resulting **SDS lysis buffer** (mixed with **RNAse A**) to each of the washed homogenised tissue sample.
15. Incubate at 55°C for 60 minutes, inverting every 10 minutes.
16. Add 4 μl or 100 μl (depending on prep size) of **Proteinase K** to each tube.
15. Incubate at 55°C for 30 minutes, inverting every 10 minutes.

### V. Protein and polysccharide removal
16. Add 1/3 volume of **5M potassium acetate** and mix by gentle inversion twice (i.e. 250 μl/2ml-prep or 6.67 ml/50ml-prep).
17. Incubate on ice or at 4°C for 10 minutes (Do not shake. As the proteins precipitate, the DNA are released from the protein-DNA complexes. The free DNA in solution are now vulnerable to shearing).
18. Centrifuge at 5,000 rcf for 5 minutes at 4°C.
19. Transfer the supernatant to a new tube, and centrifuge again at 5,000 rcf for 10 minutes at 4°C (tubes set 2).
20. Transfer the supernatant into a new tube while avoiding to include any of the pellet (tubes set 3).

### VI. Bead purification
21. Add 1 volume of the **binding buffer**, i.e. 1 ml/2ml-prep or 27 ml/50ml-prep (At this point, the DNA are getting "drawn-out" or "salted-out" of the aqueous solution and begin to condense into "sticky balls" which are now less vulnerable to shearing and are ready to stick to the magnetic beads via hydrogen bonding).
22. Add 100 μl or 1 ml (depending on the prep size) of the throroughly mixed **magnetic bead suspension**.
23. Mix by inverting the tube 20 times.
24. Incubate at room temperature with gentle mixing using a rotator or shaker platform for at least 10 minutes for 2ml-prep or at least 60 minutes for 50ml-prep.
25. Place the tube on a magnetic rack until the solution becomes clear and the bead-DNA complexes have migrated toward the magnet (This make take a few minutes to several hours depending on the DNA concentration, i.e. higher yield will take longer).
26. Keeping the tube on the magnetic rack, remove the supernatant without disturbing the beads using pipettes or by decanting.
27. Keeping the tube on the magnetic rack, fill the tubes with **70% ethanol** to wash the beads and remove any contaminants from the tube (do not distub the beads, but if the beads dislodge from the side of the tube, let them settle), and remove the ethanol using pipettes or by decanting.
29. Repeat the ethanol wash, i.e. step 27, twice.
30. Keeping the tube on the magnetic rack, remove as much of the ethanol as possible, and let the beads air dry for at most 5 minutes (Do not let the bead dry completely. They will crack and fragment the DNA which will reduce yield).
31. Remove the tube from the magnetic rack, add 50 μl or 200 μl (depending on prep size) of **nuclease-free water**, and gently tap on the tube to resuspend the DNA.
32. Incubate at room temperature for at least 10 minutes.
33. Place the tube on a magnetic rack until the solution becomes clear (This may take a few minutes to several hours depending on the DNA concentration, i.e. higher yield will take longer).
34. Transfer the supernatant (DNA in solution) to a 1.5 ml tube without disturbing and including the beads (tubes set 4).
35. Store the DNA at 4°C for up to 12 months (Do not freeze the working DNA solution to avoid freeze-thaw cycles which shears the DNA).
36. Optional step: perform a second elution. Repeat steps 31 to 34.

### Miscellaneous: quantification and quality check
- Quantify 1 μl of DNA on Nanodrop and 2 μl of DNA on Qubit fluorometer - dsDNA broad range assay (We expect at least a total 20 μg of DNA with 260/280 nm light absorbance ratio of ~ 1.8 [lower: organic contanimants; higher: RNA contamination], and 260/230 nm light absorbance ratio of ~2.0 [lower: carbohydrate contamination; higher: blank measurement error, e.g. drty pedestal or different pH]).
- Gel electrophoresis of the genomic DNA on 1% (w/v) agar at 80-150 volts for 60 minutes.
    + 1 g agar and volume up to 100 ml
    + microwave at 45-second intervals swirling the agar solution in between until the agar is fully dissolved
    + cool down for 5 minutes
    + add 2 μl of ethidium bromide or gel red
    + pour into the gel tray, place the comb, and remove any bubbles using a pipette tip
    + allow the gel to solidify for 10 minutes at 4°C or for 30 minutes at room temperature.

## References

- Mayjonade, B., J. Gouzy, C. Donnadieu, N. Pouilly, W. Marande, C. Callot, N. Langlade, S. Munos. (2016). Extraction of high-molecular-weight genomic DNA for long-read sequencing of single molecules. BioTechniques 61, 203-205.
- Jones A., C. Torkel, D. Stanley, J. Nasim, J. Borevitz, B. Schwessinger. 2020. Scalable high-molecular weight DNA extraction for long-read sequencing. [dx.doi.org/10.17504/protocols.io.bnjhmcj6](https://www.protocols.io/view/scalable-high-molecular-weight-dna-extraction-for-bnjhmcj6).
- Jones A., B. Schwessinger. 2020. Sorbitol washing complex homogenate for improved DNA extractions. [dx.doi.org/10.17504/protocols.io.beuvjew6](https://www.protocols.io/view/sorbitol-washing-complex-homogenate-for-improved-d-beuvjew6).
