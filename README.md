# DNA extraction protocols

|**Website**|**License**|
|:-------:|:--------:|
| <a href="http://bit.ly/geneplant"><img src="misc/helix_001.svg" width="75"> | [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) |
