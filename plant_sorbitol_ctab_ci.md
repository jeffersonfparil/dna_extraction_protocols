# Plant DNA Extraction using CTAB buffer and chloroform:isoamyl alcohol solution preceeded by sorbitol wash

## List of chemicals and their functions in the protocol

- **β-Mercaptoethanol** - denatures nucleases by reducing their disulfide bonds.
- **Cetrimonium bromide (CTAB)** - lyses cell membranes, and denatures proteins.
- **Chloroform (CHCl₃)** - dentaures proteins.
- **Ethanol (C₂H₅OH)** - is a hydrophilic ("water hungry") DNA washing agent.
- **Ethylenediaminetetraacetic acid (EDTA)** - chelates divalent ions which prevents nuclease activity (EDTA has more affinity to magnesium ions while EGTA (ethylene glycol-bis(β-aminoethyl ether)-N,N,N',N'-tetraacetic acid) has higher affinity to calcium ions).
- **Isoamyl alcohol (C₅H₁₂O)** - stabilises chloroform in the chloroform:isoamyl alcolohol solution.
- **Isopropanol (C₃H₈O)** - is a hydrophilic ("water hungry") alcohol used to precipitate the DNA.
- **Liquid nitrogen (N₂ at −160°C)** - is an inert cryogenic fluid used to break the plant cell wall to expose the cell membrane during tissue homogenisation or grinding.
- **Polyvinylpyrrolidone 40 (PVP 40)** - binds to polyphenols and prevents their oxidation (oxidized polyphenols binds to DNA which results in brown discoloration)
- **RNAse A** - cleaves single-stranded RNA (cleaves after pyrimidine nucleotides). Its activity is not dependent on metal ion cofactors, hence works in the presence of EDTA.
- **Sodium chloride (NaCl)** - the sodium ions (Na+) ionically binds to the negatively charged phosphate groups of the DNA backbone which induces precipitation in the presence of "water-hungry" alcohol or polyethylene glycol (PEG). This precipitation process converts DNA from a long water-coated polymer into a water-stripped condensed "sticky ball". These "sticky balls" of DNA will aggragate together after centrifugation to generate a pellet at the bottom of the tube.
- **Tris(hydroxymethyl)aminomethane hydrochloride (Tris)** - acts as buffer to keep the pH of various solutions stable at around pH 8 ± 1.
- **Water (H₂O)** - is a polar solvent. Various molecular-grade forms exist including nuclease-free water (NFW), filtered and deionized water (Milli-Q), nanopure water (nH₂O), or double distilled water (ddH₂O).

## Materials and Equipment

- mortar and pestle
- tiny spatula
- 50 ml falcon tubes
- 1.5 ml tubes
- pipettes and tips (p10, p100 & p1000)
- water bath at 60°C
- microcentrifuge
- fume hood
- - **Nuclease-free water (NFW)** and Milli-Q water or nanopure water 
- **Liquid nitrogen**
- - **RNAse A** (PureLink 20 mg/mL; [Thermo Fisher Scientific 12091039](https://www.thermofisher.com/order/catalog/product/12091039#/12091039); do not vortex since the enzymes are vulnerable to mechanical denaturation)

## Reagents

- **Stock solutions**:

| Reagent                                                 | Molecular mass (g/mol) | Preparation (autoclave & store at room temperature)                         |
|:--------------------------------------------------------|:----------------------:|:----------------------------------------------------------------------------|
| 0.25M EDTA (ethylenediaminetetraacetic acid)            | 292.24                 | 29.22g in 320ml H₂O with 12g NaOH; adjust to pH8 with NaOH; volume up 400ml |
| 1M Tris (tris(hydroxymethyl)aminomethane hydrochloride) | 121.14                 | 60.57g in 400ml H₂O; adjust to pH8 with HCl; volume up 500ml                |
| 5M NaCl (sodium chloride)                               |  58.44                 | 146g in 400ml H₂O; volume to 500ml                                          |
| 70% (v/v) Ethanol                                       |  46.07                 | 700ml in 300ml H₂O; **do not autoclave**                                    |
| 24:1 Chloroform:isoamyl alcohol                         | 119.38 and 88.15       | 24ml chloroform and add 1ml isoamyl alcohol; **do not autoclave**         |
| 100% Isopropanol                                        |  60.10                 | aliquot from container; **do not autoclave**                                |
| Water: Miili-Q and Nuclease-free water (NFW)            |  18.02                 | -                                                                           |

- **Sorbitol wash solution** (store at 4°C or autoclave and store at room temperature):

| Reagent                  | Final concentration | Amount to add for 500 ml |
|:-------------------------|--------------------:|-------------------------:|
| D-Sorbitol (182.17g/mol) |              350 mM |                  31.88 g |
| PVP (40,000 g/mol)       |                  3% |                  15.00 g |
| Tris (1 M, pH 8)         |              100 mM |                    50 ml |
| EDTA (0.25 M, pH 8)      |                5 mM |                    10 ml |
| Water (ddH₂O or Milli-Q) |                   - |      volume up to 500 ml |
| $\beta$-mercaptoethanol  |               0.1%  | 1μl/ml (add prior to use)|

- **CTAB lysis buffer** (stir at 60°C to completely dissolve CTAB):

| Reagent                 | Final concentration | Amount to add for 250 ml |
|:------------------------|--------------------:|-------------------------:|
| Tris (1 M, pH 7)        |              100 mM |                    25 ml |
| NaCl (5 M)              |              700 mM |                    35 ml |
| EDTA (0.25 M, pH 8)     |               10 mM |                    10 ml |
| PVP (40,000 g/mol)      |                  2% |                     5 g  |
| CTAB (364.45 g/mol)     |                  1% |                    2.5 g |
| Water (ddH₂O or Milli-Q)|                   - |    volume up to 249.5 ml |
| $\beta$-mercaptoethanol |                0.2% | 2μl/ml (add prior to use)|

## Procedure

### I. Prepration of buffers and solutions
1. Prepare the **stock solutions**, **sorbitol wash solution**, **CTAB lysis buffer**, and **TE buffer**.
2. Prepare 1 ml of **sorbitol wash solution**, and add 1μl β-mercaptoethanol.
3. Prepare 1 ml of **CTAB lysis buffer**, add 2μl β-mercaptoethanol, keep the cap slightly open, and pre-heat at 60°C.

### II. Homogenisation
4. Grind the plant tissues under liquid N₂ into a fine powder using mortar and pestle (top up liquid nitrogen multiple times as necessary) or ball bearings and TissueLyser (1-3 ball bearings per 2ml tube for 2min at 25Hz).
5. Transfer into 1.5ml tube (tubes set 1; should fill up to 1/3 of the tube volume. i.e. ≤ 500μl).

### III. Sorbitol wash
6. Add 1ml **sorbitol wash solution** (+1μl β-mercaptoethanol), mix throroughly by vortexing, and incubate at room temperature for 5 minutes.
7. Centrifuge at 5,000 rcf for 5 minutes at room temperature (This can be modified to 2,500 rcf for 10 min at room temperature to avoid the ball bearings rupturing the tubes).
8. Keep the pellet and remove the supernatant by decanting (The supernatant should be cloudy and light yellow or light brown. It is fine to lose some of the floating plant tissues).
9. If the supernatant is viscous and dark in colour, repeat the sorbitol wash (i.e. repeat from step 6).

### IV. Cell lysis and RNAse treatment
10. Add 1ml of pre-heated **CTAB lysis buffer** (+2μl β-mercaptoethanol), and mix thoroughly by vortexing.
11. Add 10 μl of **RNAse A** and mix gently by inversion.
12. Incubate at 60°C for 30 minutes, inverting every 10 minutes.
13. Centrifuge at 10,000 rcf for 2 minutes
14. Transfer 750μl of aqueous layer into a new 1.5ml tube taking care not to include any leaf tissues

### V. Protein denaturation
15. Add 750μl (equal volume) of 24:1 chloroform:isoamyl alcohol, mix by gentle inversion, and centrifuge at 5,000 rcf for 10 minutes.
16. Using a wide-bore p1000 pipette tip, transfer 500μl of the uppermost aqueous phase into a fresh 1.5ml tube, taking care not to include any of the interphase which contains the denatured proteins.

### VI. DNA precipitation
17. Add 100μl (1/5 volume) of 5M NaCl and 500μl (equal volume) of cold isopropanol, mix by gentle inversion, and incubate at room temperature for 30 minutes.
18. Centrifuge at 5,000g  for 30 minutes at 4°C (non-refrigirated centrifuge running inside a cold room at 4°C).
18. Discard the supernatant, add 1 ml of cold 70% ethanol, clean the pellet and the tube by inversion, and discard the supernatant.
20. Repeat the ethanol wash 2 more times, i.e. perform step 13 for a total of 3 times or more depending on the appearance of the pellet (the pellet should be transluscent to slightly whitish).
21. Discard supernatant, remove all traces of ethanol, and dry the pellet under the fume hood for not more than 10 minutes.
22. Dissolve DNA in 28μl nuclease-free water.

### Miscellaneous: quantification and quality check
- Quantify 1 μl of DNA on Nanodrop and 2 μl of DNA on Qubit fluorometer - dsDNA broad range assay (We expect at least a total 20 μg of DNA with 260/280 nm light absorbance ratio of ~ 1.8 [lower: organic contanimants; higher: RNA contamination], and 260/230 nm light absorbance ratio of ~2.0 [lower: carbohydrate contamination; higher: blank measurement error, e.g. dirty pedestal or different pH]).
- Gel electrophoresis of the genomic DNA on 1% (w/v) agar at 80-150 volts for 60 minutes.
    + 1 g agar and volume up to 100 ml
    + microwave at 45-second intervals swirling the agar solution in between until the agar is fully dissolved
    + cool down for 5 minutes
    + add 2 μl of ethidium bromide or gel red
    + pour into the gel tray, place the comb, and remove any bubbles using a pipette tip
    + allow the gel to solidify for 10 minutes at 4°C or for 30 minutes at room temperature.
