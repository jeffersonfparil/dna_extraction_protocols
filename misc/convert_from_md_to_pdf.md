# Install pandoc
- [https://pandoc.org/installing.html](https://pandoc.org/installing.html)
- For example in ubuntu: `sudo apt install texlive-latex-base pandoc texlive-fonts-recommended`

# Convert to a markdown file with special characters:
- `pandoc test.md --latex-engine=xelatex -o test.pdf`

# Convert to a markdown file into other formats without special characters:
- `pandoc test.md -o test.txt`
- `pandoc test.md -o test.html`
- `pandoc test.md -o test.pdf`
- `pandoc test.md -o test.doc`
- `pandoc test.md -o test.docx`
