# Plant DNA Extraction using CTAB buffer and chloroform:isoamyl alcohol solution

## List of chemicals and their functions in the protocol

- **β-Mercaptoethanol** - denatures nucleases by reducing their disulfide bonds.
- **Cetrimonium bromide (CTAB)** - lyses cell membranes, and denatures proteins.
- **Chloroform (CHCl₃)** - dentaures proteins.
- **Ethanol (C₂H₅OH)** - is a hydrophilic ("water hungry") DNA washing agent.
- **Ethylenediaminetetraacetic acid (EDTA)** - chelates divalent ions which prevents nuclease activity (EDTA has more affinity to magnesium ions while EGTA (ethylene glycol-bis(β-aminoethyl ether)-N,N,N',N'-tetraacetic acid) has higher affinity to calcium ions).
- **Isoamyl alcohol (C₅H₁₂O)** - stabilises chloroform in the chloroform:isoamyl alcolohol solution.
- **Isopropanol (C₃H₈O)** - is a hydrophilic ("water hungry") alcohol used to precipitate the DNA.
- **Liquid nitrogen (N₂ at −160°C)** - is an inert cryogenic fluid used to break the plant cell wall to expose the cell membrane during tissue homogenisation or grinding.
- **Sodium chloride (NaCl)** - the sodium ions (Na+) ionically binds to the negatively charged phosphate groups of the DNA backbone which induces precipitation in the presence of "water-hungry" alcohol or polyethylene glycol (PEG). This precipitation process converts DNA from a long water-coated polymer into a water-stripped condensed "sticky ball". These "sticky balls" of DNA will aggragate together after centrifugation to generate a pellet at the bottom of the tube.
- **Tris(hydroxymethyl)aminomethane hydrochloride (Tris)** - acts as buffer to keep the pH of various solutions stable at around pH 8 ± 1.
- **Water (H₂O)** - is a polar solvent. Various molecular-grade forms exist including nuclease-free water (NFW), filtered and deionized water (Milli-Q), nanopure water (nH₂O), or double distilled water (ddH₂O).

## Materials and Equipment

- mortar and pestle
- tiny spatula
- 50 ml falcon tubes
- 1.5 ml tubes
- pipettes and tips (p10, p100 & p1000)
- water bath at 60&deg;C-65&deg;C (heat block + 1L glass beaker)
- microcentrifuge
- fume hood

## Reagents
- CTAB DNA extraction buffer

| Reagent                 | Final concentration | Amount to add for 250 ml |
|:------------------------|--------------------:|-------------------------:|
| Tris (1 M, pH 7)        |              100 mM |                    25 ml |
| NaCl (5 M)              |              700 mM |                    35 ml |
| EDTA (0.25 M, pH 8)     |               10 mM |                    10 ml |
| CTAB (364.45 g/mol)     |                  1% |                    2.5 g |
| $H_{2}O$                |                   - |    volume up to 249.5 ml |
| $\beta$-mercaptoethanol |                0.2% |                   0.5 ml |

where:

| Reagent    | Molecular mass | Directions (autoclave & store at room temperature) |
|:-----------|:--------------:|:---------------------------------------------------|
| 1M Tris    |  121.14g/mol   | 60.57g in 400ml; pH7 with HCl; volume up 500ml     |
| 5M NaCl    |   58.4g/mol    | 146g in 400ml; volume to 500ml                     |
| 0.25M EDTA |  292.24g/mol   | 29.22g in 320ml with 4g NaOH; pH8; volume up 400ml |

- Chloroform:isoamyl alcohol (24:1)
- Ethanol (70%)
- Isopropanol
- TE buffer (pH 8)

| Reagent             | Final concentration | Amount to add for 250 ml |
|:--------------------|--------------------:|-------------------------:|
| Tris (1 M, pH 7)    |               10 mM |                   2.5 ml |
| EDTA (0.25 M, pH 8) |                1 mM |                     1 ml |

## Procedure

1. Prepare leaf tissues (pool ~100 samples of ~100mg or ~1cm^2^ leaf tissues each, for a total of ~10g into a 50-ml falcon tube)
2. **Fume hood**: Prepare 10 ml of CTAB in a 50ml-falcon tube (~10 ml CTAB + 20 $\mu$l $\beta$-mercaptoethanol), do not close cap tightly and heat up in water bath at 60&deg;C - 65&deg;C
3. Grind leaf tissues into fine powder with mortar and pestle in liquid nitrogen (top up liquid nitrogen multiple times as necessary)
4. Place ~100 $\mu$l of powdered leaf tissues (~5 spatula-fulls) into each 1.5ml tube (do not close)
5. **Fume hood**: Add 1.0ml of pre-heated CTAB solution into the 1.5ml tube containing the powdered leaf tissues, close the lid once the leaf tissues have thawed in the solution, and mix by inversion
6. **Fume hood**: Incubate the CTAB-leaf sludge at 60&deg;C - 65&deg;C for 30 minutes while mixing every 10 minutes [lyse the cells] + Note: During the first mixing, open and close the caps to equilibrate pressures
7. Centrifuge at 10,000g $\big( RPM = \sqrt{g \over {(1.118 \times 10^{-5}) \times rotor\_radius (cm)}}\big)$ for 2 minutes
8. **Fume hood**: Transfer 750$\mu$l of aqueous layer into a new 1.5ml tube taking care not to include any leaf tissues
9. **Fume hood**: Add 750$\mu$l (equal volume) of 24:1 chloroform:isoamyl alcohol, mix by gentle inversion, and centrifuge at 10,000g for 5 minutes [denature and separate the proteins]
10. **Fume hood**: Transfer 500$\mu$l of the uppermost aqueous phase into a fresh 1.5ml tube, taking care not to include any of the interphase [isolate nucleic acids (both DNA and RNA) from the denatured proteins]
11. Add 100$\mu$l (1/5 volume) of 5M NaCl and 500$\mu$l (equal volume) of cold isopropanol, mix by gentle inversion, and incubate at room temperature for 30 minutes [precipitate nucleic acids]
12. Centrifuge at 10,000g  for 15 minutes at room temperature [pellet the nucleic acids]
13. Discard the supernatant, add 1 ml of cold 70% ethanol, clean the pellet and the tube by inversion, and discard the supernatant
14. Add 500$\mu$l of cold 70% ethanol, and centrifuge at 10,000g for 2 minutes
15. **Fume hood**: Discard supernatant, and dry the pellet under the fume hood for about 15 minutes
16. Dissolve DNA in 22$\mu$l nuclease-free water (1$\mu$l for Nanodrop quality check and 1$\mu$l for Qubit DNA quantification)

## Reference

- Doyle, J.J. and Doyle, J.L. (1987) A rapid DNA isolation procedure for small quantities of fresh leaf tissue. Phytochemical Bulletin, 19, 11-15. 